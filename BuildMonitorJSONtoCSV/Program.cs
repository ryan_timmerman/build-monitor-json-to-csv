﻿using BuildMonitorJSONtoCSV.CommandLine;
using BuildMonitorJSONtoCSV.Parsers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitorJSONtoCSV
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				var arguments = CommandLineParser.ParseArgs(args);

				var inputStream = new StreamReader(arguments.SourceFile);

				var reader = new BuildMonitorReader(inputStream);

				var outputStream = new StreamWriter(arguments.OutputFile);
				var writer = new BuildMonitorWriter(outputStream);

				foreach(var solution in reader.GetSolutions())
				{
					writer.WriteSolution(solution);
				}

			}
			catch(Exception ex)
			{
				Console.WriteLine(ex);
			}

			Console.WriteLine("Press any key to continue");
			Console.ReadKey();
		}
	}
}
