﻿using BuildMonitorJSONtoCSV.Data;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitorJSONtoCSV.Parsers
{
	public class BuildMonitorWriter
	{
		private CsvWriter _Writer;

		public BuildMonitorWriter(StreamWriter outputStream)
		{
			_Writer = new CsvWriter(outputStream);
		}

		public void WriteSolution(SolutionBuild solution)
		{
			foreach(var project in solution.Projects)
			{
				_Writer.WriteField(solution.SolutionStart);
				_Writer.WriteField(solution.SolutionName);
				_Writer.WriteField(solution.SolutionDuration);
				_Writer.WriteField(project.ProjectStart);
				_Writer.WriteField(project.ProjectDuration);
				_Writer.WriteField(project.ProjectName);
				_Writer.NextRecord();
			}
		}
	}
}
