﻿using BuildMonitorJSONtoCSV.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitorJSONtoCSV.Parsers
{
	public class BuildMonitorReader
	{
		private StreamReader Source { get; set; }

		public BuildMonitorReader(StreamReader reader)
		{
			Source = reader;
		}

		public IEnumerable<SolutionBuild> GetSolutions()
		{
			JsonTextReader reader = new JsonTextReader(Source);

		/*	while (reader.Read())
			{
				if (reader.Value != null)
				{
					Console.WriteLine("Token: {0}, Value: {1}", reader.TokenType, reader.Value);
				}
				else
				{
					Console.WriteLine("Token: {0}", reader.TokenType);
				}
			}*/

			//This is going to be messy, will no doubt have to refactor later
			reader.Read();  //Start array
			reader.Read();  //Start object

			do
			{
				SolutionBuild solution = new SolutionBuild();

				//Solution build start time
				reader.Read();
				solution.SolutionStart = reader.ReadAsDateTime() ?? DateTime.MinValue;

				//Solution build time
				reader.Read();
				solution.SolutionDuration = reader.ReadAsInt32() ?? -1;

				reader.Read();

				//The name of the solution
				var dataType = reader.Value as string;

				//Sometimes the solution name is not there
				if(dataType == "Solution")
				{
					reader.Read(); //Start object
					reader.Read(); //Name property
					solution.SolutionName = reader.ReadAsString();
					reader.Read(); //End object

					reader.Read(); //Projects header
				}
				else
				{
					solution.SolutionName = "Not Applicable";
				}

				reader.Read(); //Start array

				reader.Read(); //Start object (or EndArray if there are no projects)

				while(reader.TokenType != JsonToken.EndArray)
				{
					ProjectBuild project = new ProjectBuild();

					reader.Read(); //Start time
					project.ProjectStart = reader.ReadAsDateTime() ?? DateTime.MinValue;

					reader.Read(); //Time elapsed
					project.ProjectDuration = reader.ReadAsInt32() ?? -1;

					reader.Read();
					reader.Read();
					reader.Read(); //Project name
					project.ProjectName = reader.ReadAsString();

					reader.Read(); //Project GUID
					reader.Read();
					reader.Read(); //End object
					reader.Read(); //End object

					solution.Projects.Add(project);

					reader.Read();  //Start object or end array
				};

				yield return solution;

				reader.Read(); //End object
				reader.Read(); //Either new object or end array
			} while (reader.TokenType != JsonToken.EndArray);

			yield break;
		}
	}
}
