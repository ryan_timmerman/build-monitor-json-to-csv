﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitorJSONtoCSV
{
	public class Parameters
	{
		public string SourceFile { get; set; }
		public string OutputFile { get; set; }

		//More potential options include being able to filter out specific categories
	}
}
