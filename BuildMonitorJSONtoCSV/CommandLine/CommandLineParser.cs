﻿using Microsoft.Test.CommandLineParsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitorJSONtoCSV.CommandLine
{
	internal static class CommandLineParser
	{
		public static Parameters ParseArgs(string[] args)
		{
			var parser = CommandLineDictionary.FromArguments(args);

			var param = new Parameters();

			param.SourceFile = parser["source"];
			param.OutputFile = parser["dest"];

			return param;
		}
	}
}
