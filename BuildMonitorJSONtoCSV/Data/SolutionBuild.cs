﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitorJSONtoCSV.Data
{
	public class SolutionBuild
	{
		public DateTime SolutionStart { get; set; }
		public int SolutionDuration { get; set; }
		public string SolutionName { get; set; }

		public List<ProjectBuild> Projects { get; set; }

		public SolutionBuild()
		{
			Projects = new List<ProjectBuild>();
		}
	}
}
