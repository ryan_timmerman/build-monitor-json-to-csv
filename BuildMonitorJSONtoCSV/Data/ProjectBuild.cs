﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitorJSONtoCSV.Data
{
	public class ProjectBuild
	{
		public DateTime ProjectStart { get; set; }
		public int ProjectDuration { get; set; }
		public string ProjectName { get; set; }
	}
}
